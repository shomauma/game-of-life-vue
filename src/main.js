import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import svgJs from 'svg.js'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use({
  install: v => {
    v.prototype.$svg = svgJs
  }
})

new Vue({
  render: h => h(App)
}).$mount('#app')
